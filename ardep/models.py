from django.urls import reverse
from django.db import models as models


class EkranyGaty(models.Model):

    VISIBLE_CHOICES = [
        ('0', 'NIE'),
        ('1', 'TAK'),
    ]

    # Fields
    gate = models.CharField(max_length=100, verbose_name='Brama')
    direction = models.CharField(max_length=100, verbose_name='Kierunek')
    status = models.CharField(max_length=100, verbose_name='Status')
    info = models.CharField(max_length=100, verbose_name='Informacja')
    carrier = models.CharField(max_length=100, verbose_name='Przewoźnik')
    flight = models.CharField(max_length=30, verbose_name='Numer Lotu')
    visible = models.CharField(max_length=1, default=0, choices=VISIBLE_CHOICES, verbose_name='Widoczny')
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Utworzony')
    last_updated = models.DateTimeField(auto_now=True, editable=False, verbose_name='Edytowany')


    class Meta:
        verbose_name = 'Ekran Gaty-u'
        verbose_name_plural = 'Ekrany Gate-ów'
        ordering = ('-created',)

    def __str__(self):
        return u'%s' % self.direction


class KontrolaBezpieczenstwa(models.Model):

    VISIBLE_CHOICES = [
        ('0', 'NIE'),
        ('1', 'TAK'),
    ]

    # Fields
    gate = models.CharField(max_length=100, verbose_name='Brama')
    direction = models.CharField(max_length=100, verbose_name='Kierunek')
    status = models.CharField(max_length=100, verbose_name='Status')
    info = models.CharField(max_length=100, verbose_name='Informacja')
    carrier = models.CharField(max_length=100, verbose_name='Przewoźnik')
    flight = models.CharField(max_length=30, verbose_name='Numer Lotu')
    visible = models.CharField(max_length=1, default=0, choices=VISIBLE_CHOICES, verbose_name='Widoczny')
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Utworzony')
    last_updated = models.DateTimeField(auto_now=True, editable=False, verbose_name='Edytowany')


    class Meta:
        verbose_name = 'Kontrola Bezpieczeństwa'
        verbose_name_plural = 'Kontrola Bezpieczeństwa'
        ordering = ('-created',)

    def __str__(self):
        return u'%s' % self.direction


class MalaKaruzela(models.Model):

    VISIBLE_CHOICES = [
        ('0', 'NIE'),
        ('1', 'TAK'),
    ]

    # Fields
    direction = models.CharField(max_length=100, verbose_name='Kierunek')
    flight = models.CharField(max_length=30, verbose_name='Numer Lotu')
    visible = models.CharField(max_length=1, default=0, choices=VISIBLE_CHOICES, verbose_name='Widoczny')
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Utworzony')
    last_updated = models.DateTimeField(auto_now=True, editable=False, verbose_name='Edytowany')


    class Meta:
        verbose_name = 'Mała Karuzela'
        verbose_name_plural = 'Mała Karuzela'
        ordering = ('-created',)

    def __str__(self):
        return u'%s' % self.direction


class DuzaKaruzela(models.Model):

    VISIBLE_CHOICES = [
        ('0', 'NIE'),
        ('1', 'TAK'),
    ]

    # Fields
    direction = models.CharField(max_length=100, verbose_name='Kierunek')
    flight = models.CharField(max_length=30, verbose_name='Numer Lotu')
    visible = models.CharField(max_length=1, default=0, choices=VISIBLE_CHOICES, verbose_name='Widoczny')
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Utworzony')
    last_updated = models.DateTimeField(auto_now=True, editable=False, verbose_name='Edytowany')


    class Meta:
        verbose_name = 'Duża Karuzela'
        verbose_name_plural = 'Duża Karuzela'
        ordering = ('-created',)

    def __str__(self):
        return u'%s' % self.direction


class NumeryGatow(models.Model):

    VISIBLE_CHOICES = [
        ('0', 'NIE'),
        ('1', 'TAK'),
    ]

    # Fields
    direction = models.CharField(max_length=100, verbose_name='Kierunek')
    gate = models.CharField(max_length=100, verbose_name='Brama')
    visible = models.CharField(max_length=1, default=0, choices=VISIBLE_CHOICES, verbose_name='Widoczny')
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Utworzony')
    last_updated = models.DateTimeField(auto_now=True, editable=False, verbose_name='Edytowany')


    class Meta:
        verbose_name = 'Numer Gate-u'
        verbose_name_plural = 'Numery Gate-ów'
        ordering = ('-created',)

    def __str__(self):
        return u'%s' % self.direction