from django.urls import path
from django.contrib import admin
from . import views

urlpatterns = (
    # urls for EkranyGaty
    path('app_name/ekranygaty/', views.EkranyGatyListView.as_view(), name='app_name_ekranygaty_list'),
    path('app_name/ekranygaty/create/', views.EkranyGatyCreateView.as_view(), name='app_name_ekranygaty_create'),
    path('app_name/ekranygaty/detail/<int:pk>/', views.EkranyGatyDetailView.as_view(), name='app_name_ekranygaty_detail'),
    path('app_name/ekranygaty/update/<int:pk>/', views.EkranyGatyUpdateView.as_view(), name='app_name_ekranygaty_update'),
)

urlpatterns += (
    # urls for KontrolaBezpieczenstwa
    path('app_name/kontrolabezpieczenstwa/', views.KontrolaBezpieczenstwaListView.as_view(), name='app_name_kontrolabezpieczenstwa_list'),
    path('app_name/kontrolabezpieczenstwa/create/', views.KontrolaBezpieczenstwaCreateView.as_view(), name='app_name_kontrolabezpieczenstwa_create'),
    path('app_name/kontrolabezpieczenstwa/detail/<int:pk>/', views.KontrolaBezpieczenstwaDetailView.as_view(), name='app_name_kontrolabezpieczenstwa_detail'),
    path('app_name/kontrolabezpieczenstwa/update/<int:pk>/', views.KontrolaBezpieczenstwaUpdateView.as_view(), name='app_name_kontrolabezpieczenstwa_update'),
)

urlpatterns += (
    # urls for MalaKaruzela
    path('app_name/malakaruzela/', views.MalaKaruzelaListView.as_view(), name='app_name_malakaruzela_list'),
    path('app_name/malakaruzela/create/', views.MalaKaruzelaCreateView.as_view(), name='app_name_malakaruzela_create'),
    path('app_name/malakaruzela/detail/<int:pk>/', views.MalaKaruzelaDetailView.as_view(), name='app_name_malakaruzela_detail'),
    path('app_name/malakaruzela/update/<int:pk>/', views.MalaKaruzelaUpdateView.as_view(), name='app_name_malakaruzela_update'),
)

urlpatterns += (
    # urls for DuzaKaruzela
    path('app_name/duzakaruzela/', views.DuzaKaruzelaListView.as_view(), name='app_name_duzakaruzela_list'),
    path('app_name/duzakaruzela/create/', views.DuzaKaruzelaCreateView.as_view(), name='app_name_duzakaruzela_create'),
    path('app_name/duzakaruzela/detail/<int:pk>/', views.DuzaKaruzelaDetailView.as_view(), name='app_name_duzakaruzela_detail'),
    path('app_name/duzakaruzela/update/<int:pk>/', views.DuzaKaruzelaUpdateView.as_view(), name='app_name_duzakaruzela_update'),
)

urlpatterns += (
    # urls for NumeryGatow
    path('app_name/numerygatow/', views.NumeryGatowListView.as_view(), name='app_name_numerygatow_list'),
    path('app_name/numerygatow/create/', views.NumeryGatowCreateView.as_view(), name='app_name_numerygatow_create'),
    path('app_name/numerygatow/detail/<int:pk>/', views.NumeryGatowDetailView.as_view(), name='app_name_numerygatow_detail'),
    path('app_name/numerygatow/update/<int:pk>/', views.NumeryGatowUpdateView.as_view(), name='app_name_numerygatow_update'),
)

admin.site.site_header = "Loty Szczecin"
admin.site.site_title = "Loty Szczecin Portal"
admin.site.index_title = "Witaj w portalu Loty Sczecin"
