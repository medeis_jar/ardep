import unittest
from django.urls import reverse
from django.test import Client
from .models import EkranyGaty, KontrolaBezpieczenstwa, MalaKaruzela, DuzaKaruzela, NumeryGatow
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType


def create_django_contrib_auth_models_user(**kwargs):
    defaults = {}
    defaults["username"] = "username"
    defaults["email"] = "username@tempurl.com"
    defaults.update(**kwargs)
    return User.objects.create(**defaults)


def create_django_contrib_auth_models_group(**kwargs):
    defaults = {}
    defaults["name"] = "group"
    defaults.update(**kwargs)
    return Group.objects.create(**defaults)


def create_django_contrib_contenttypes_models_contenttype(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return ContentType.objects.create(**defaults)


def create_ekranygaty(**kwargs):
    defaults = {}
    defaults["gate"] = "gate"
    defaults["direction"] = "direction"
    defaults["status"] = "status"
    defaults["info"] = "info"
    defaults["carrier"] = "carrier"
    defaults["flight"] = "flight"
    defaults["visible"] = "visible"
    defaults.update(**kwargs)
    return EkranyGaty.objects.create(**defaults)


def create_kontrolabezpieczenstwa(**kwargs):
    defaults = {}
    defaults["gate"] = "gate"
    defaults["direction"] = "direction"
    defaults["status"] = "status"
    defaults["info"] = "info"
    defaults["carrier"] = "carrier"
    defaults["flight"] = "flight"
    defaults["visible"] = "visible"
    defaults.update(**kwargs)
    return KontrolaBezpieczenstwa.objects.create(**defaults)


def create_malakaruzela(**kwargs):
    defaults = {}
    defaults["direction"] = "direction"
    defaults["flight"] = "flight"
    defaults["visible"] = "visible"
    defaults.update(**kwargs)
    return MalaKaruzela.objects.create(**defaults)


def create_duzakaruzela(**kwargs):
    defaults = {}
    defaults["direction"] = "direction"
    defaults["flight"] = "flight"
    defaults["visible"] = "visible"
    defaults.update(**kwargs)
    return DuzaKaruzela.objects.create(**defaults)


def create_numerygatow(**kwargs):
    defaults = {}
    defaults["gate"] = "gate"
    defaults["direction"] = "direction"
    defaults["visible"] = "visible"
    defaults.update(**kwargs)
    return NumeryGatow.objects.create(**defaults)


class EkranyGatyViewTest(unittest.TestCase):
    '''
    Tests for EkranyGaty
    '''
    def setUp(self):
        self.client = Client()

    def test_list_ekranygaty(self):
        url = reverse('app_name_ekranygaty_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_ekranygaty(self):
        url = reverse('app_name_ekranygaty_create')
        data = {
            "gate": "gate",
            "direction": "direction",
            "status": "status",
            "info": "info",
            "carrier": "carrier",
            "flight": "flight",
            "visible": "visible",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_ekranygaty(self):
        ekranygaty = create_ekranygaty()
        url = reverse('app_name_ekranygaty_detail', args=[ekranygaty.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_ekranygaty(self):
        ekranygaty = create_ekranygaty()
        data = {
            "gate": "gate",
            "direction": "direction",
            "status": "status",
            "info": "info",
            "carrier": "carrier",
            "flight": "flight",
            "visible": "visible",
        }
        url = reverse('app_name_ekranygaty_update', args=[ekranygaty.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class KontrolaBezpieczenstwaViewTest(unittest.TestCase):
    '''
    Tests for KontrolaBezpieczenstwa
    '''
    def setUp(self):
        self.client = Client()

    def test_list_kontrolabezpieczenstwa(self):
        url = reverse('app_name_kontrolabezpieczenstwa_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_kontrolabezpieczenstwa(self):
        url = reverse('app_name_kontrolabezpieczenstwa_create')
        data = {
            "gate": "gate",
            "direction": "direction",
            "status": "status",
            "info": "info",
            "carrier": "carrier",
            "flight": "flight",
            "visible": "visible",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_kontrolabezpieczenstwa(self):
        kontrolabezpieczenstwa = create_kontrolabezpieczenstwa()
        url = reverse('app_name_kontrolabezpieczenstwa_detail', args=[kontrolabezpieczenstwa.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_kontrolabezpieczenstwa(self):
        kontrolabezpieczenstwa = create_kontrolabezpieczenstwa()
        data = {
            "gate": "gate",
            "direction": "direction",
            "status": "status",
            "info": "info",
            "carrier": "carrier",
            "flight": "flight",
            "visible": "visible",
        }
        url = reverse('app_name_kontrolabezpieczenstwa_update', args=[kontrolabezpieczenstwa.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class MalaKaruzelaViewTest(unittest.TestCase):
    '''
    Tests for MalaKaruzela
    '''
    def setUp(self):
        self.client = Client()

    def test_list_malakaruzela(self):
        url = reverse('app_name_malakaruzela_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_malakaruzela(self):
        url = reverse('app_name_malakaruzela_create')
        data = {
            "direction": "direction",
            "flight": "flight",
            "visible": "visible",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_malakaruzela(self):
        malakaruzela = create_malakaruzela()
        url = reverse('app_name_malakaruzela_detail', args=[malakaruzela.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_malakaruzela(self):
        malakaruzela = create_malakaruzela()
        data = {
            "direction": "direction",
            "flight": "flight",
            "visible": "visible",
        }
        url = reverse('app_name_malakaruzela_update', args=[malakaruzela.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class DuzaKaruzelaViewTest(unittest.TestCase):
    '''
    Tests for DuzaKaruzela
    '''
    def setUp(self):
        self.client = Client()

    def test_list_duzakaruzela(self):
        url = reverse('app_name_duzakaruzela_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_duzakaruzela(self):
        url = reverse('app_name_duzakaruzela_create')
        data = {
            "direction": "direction",
            "flight": "flight",
            "visible": "visible",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_duzakaruzela(self):
        duzakaruzela = create_duzakaruzela()
        url = reverse('app_name_duzakaruzela_detail', args=[duzakaruzela.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_duzakaruzela(self):
        duzakaruzela = create_duzakaruzela()
        data = {
            "direction": "direction",
            "flight": "flight",
            "visible": "visible",
        }
        url = reverse('app_name_duzakaruzela_update', args=[duzakaruzela.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class NumeryGatowViewTest(unittest.TestCase):
    '''
    Tests for NumeryGatow
    '''
    def setUp(self):
        self.client = Client()

    def test_list_numerygatow(self):
        url = reverse('app_name_numerygatow_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_numerygatow(self):
        url = reverse('app_name_numerygatow_create')
        data = {
            "gate": "gate",
            "direction": "direction",
            "visible": "visible",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_numerygatow(self):
        numerygatow = create_numerygatow()
        url = reverse('app_name_numerygatow_detail', args=[numerygatow.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_numerygatow(self):
        numerygatow = create_numerygatow()
        data = {
            "gate": "gate",
            "direction": "direction",
            "visible": "visible",
        }
        url = reverse('app_name_numerygatow_update', args=[numerygatow.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)