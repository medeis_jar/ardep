from django.views.generic import DetailView, ListView, UpdateView, CreateView
from .models import EkranyGaty, KontrolaBezpieczenstwa, MalaKaruzela, DuzaKaruzela, NumeryGatow
from .forms import EkranyGatyForm, KontrolaBezpieczenstwaForm, MalaKaruzelaForm, DuzaKaruzelaForm, NumeryGatowForm


class EkranyGatyListView(ListView):
    model = EkranyGaty


class EkranyGatyCreateView(CreateView):
    model = EkranyGaty
    form_class = EkranyGatyForm


class EkranyGatyDetailView(DetailView):
    model = EkranyGaty


class EkranyGatyUpdateView(UpdateView):
    model = EkranyGaty
    form_class = EkranyGatyForm


class KontrolaBezpieczenstwaListView(ListView):
    model = KontrolaBezpieczenstwa


class KontrolaBezpieczenstwaCreateView(CreateView):
    model = KontrolaBezpieczenstwa
    form_class = KontrolaBezpieczenstwaForm


class KontrolaBezpieczenstwaDetailView(DetailView):
    model = KontrolaBezpieczenstwa


class KontrolaBezpieczenstwaUpdateView(UpdateView):
    model = KontrolaBezpieczenstwa
    form_class = KontrolaBezpieczenstwaForm


class MalaKaruzelaListView(ListView):
    model = MalaKaruzela


class MalaKaruzelaCreateView(CreateView):
    model = MalaKaruzela
    form_class = MalaKaruzelaForm


class MalaKaruzelaDetailView(DetailView):
    model = MalaKaruzela


class MalaKaruzelaUpdateView(UpdateView):
    model = MalaKaruzela
    form_class = MalaKaruzelaForm


class DuzaKaruzelaListView(ListView):
    model = DuzaKaruzela


class DuzaKaruzelaCreateView(CreateView):
    model = DuzaKaruzela
    form_class = DuzaKaruzelaForm


class DuzaKaruzelaDetailView(DetailView):
    model = DuzaKaruzela


class DuzaKaruzelaUpdateView(UpdateView):
    model = DuzaKaruzela
    form_class = DuzaKaruzelaForm


class NumeryGatowListView(ListView):
    model = NumeryGatow


class NumeryGatowCreateView(CreateView):
    model = NumeryGatow
    form_class = NumeryGatowForm


class NumeryGatowDetailView(DetailView):
    model = NumeryGatow


class NumeryGatowUpdateView(UpdateView):
    model = NumeryGatow
    form_class = NumeryGatowForm



