from django.contrib import admin
from django import forms
from .models import EkranyGaty, KontrolaBezpieczenstwa, MalaKaruzela, DuzaKaruzela, NumeryGatow


class EkranyGatyAdminForm(forms.ModelForm):

    class Meta:
        model = EkranyGaty
        fields = '__all__'


class EkranyGatyAdmin(admin.ModelAdmin):
    form = EkranyGatyAdminForm
    list_display = ['gate', 'direction', 'status', 'info', 'carrier', 'flight', 'visible', 'created', 'last_updated']
    # readonly_fields = ['gate', 'direction', 'status', 'info', 'carrier', 'flight', 'visible', 'created', 'last_updated']


admin.site.register(EkranyGaty, EkranyGatyAdmin)


class KontrolaBezpieczenstwaAdminForm(forms.ModelForm):

    class Meta:
        model = KontrolaBezpieczenstwa
        fields = '__all__'


class KontrolaBezpieczenstwaAdmin(admin.ModelAdmin):
    form = KontrolaBezpieczenstwaAdminForm
    list_display = ['gate', 'direction', 'status', 'info', 'carrier', 'flight', 'visible', 'created', 'last_updated']
    # readonly_fields = ['gate', 'direction', 'status', 'info', 'carrier', 'flight', 'visible', 'created', 'last_updated']


admin.site.register(KontrolaBezpieczenstwa, KontrolaBezpieczenstwaAdmin)


class MalaKaruzelaAdminForm(forms.ModelForm):

    class Meta:
        model = MalaKaruzela
        fields = '__all__'


class MalaKaruzelaAdmin(admin.ModelAdmin):
    form = MalaKaruzelaAdminForm
    list_display = ['direction', 'flight', 'visible', 'created', 'last_updated']
    # readonly_fields = ['direction', 'flight', 'visible', 'created', 'last_updated']


admin.site.register(MalaKaruzela, MalaKaruzelaAdmin)


class DuzaKaruzelaAdminForm(forms.ModelForm):

    class Meta:
        model = DuzaKaruzela
        fields = '__all__'


class DuzaKaruzelaAdmin(admin.ModelAdmin):
    form = DuzaKaruzelaAdminForm
    list_display = ['direction', 'flight', 'visible', 'created', 'last_updated']
    # readonly_fields = ['direction', 'flight', 'visible', 'created', 'last_updated']


admin.site.register(DuzaKaruzela, DuzaKaruzelaAdmin)


class NumeryGatowAdminForm(forms.ModelForm):

    class Meta:
        model = NumeryGatow
        fields = '__all__'


class NumeryGatowAdmin(admin.ModelAdmin):
    form = NumeryGatowAdminForm
    list_display = ['direction', 'gate', 'visible', 'created', 'last_updated']
    # readonly_fields = ['gate', 'direction', 'visible', 'created', 'last_updated']


admin.site.register(NumeryGatow, NumeryGatowAdmin)


