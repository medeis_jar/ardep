# Generated by Django 2.1.5 on 2019-06-23 09:59

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Flight',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('gate', models.CharField(max_length=100, verbose_name='Brama')),
                ('direction', models.CharField(max_length=100, verbose_name='Kierunek')),
                ('status', models.CharField(max_length=100, verbose_name='Status')),
                ('info', models.CharField(max_length=100, verbose_name='Informacja')),
                ('carrier', models.CharField(max_length=100, verbose_name='Przewoźnik')),
                ('flight', models.CharField(max_length=30, verbose_name='Numer Lotu')),
                ('visible', models.CharField(choices=[('0', 'NIE'), ('1', 'TAK')], default=0, max_length=1, verbose_name='Widoczny')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Utworzony')),
                ('last_updated', models.DateTimeField(auto_now=True, verbose_name='Edytowany')),
            ],
            options={
                'verbose_name': 'Lot',
                'verbose_name_plural': 'Loty',
                'ordering': ('-created',),
            },
        ),
    ]
