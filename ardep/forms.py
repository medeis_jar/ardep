from django import forms
from .models import EkranyGaty, KontrolaBezpieczenstwa, MalaKaruzela, DuzaKaruzela, NumeryGatow


class EkranyGatyForm(forms.ModelForm):
    class Meta:
        model = EkranyGaty
        fields = ['gate', 'direction', 'status', 'info', 'carrier', 'flight', 'visible']


class KontrolaBezpieczenstwaForm(forms.ModelForm):
    class Meta:
        model = KontrolaBezpieczenstwa
        fields = ['gate', 'direction', 'status', 'info', 'carrier', 'flight', 'visible']


class MalaKaruzelaForm(forms.ModelForm):
    class Meta:
        model = MalaKaruzela
        fields = ['direction', 'flight', 'visible']


class DuzaKaruzelaForm(forms.ModelForm):
    class Meta:
        model = DuzaKaruzela
        fields = ['direction', 'flight', 'visible']


class NumeryGatowForm(forms.ModelForm):
    class Meta:
        model = NumeryGatow
        fields = ['gate', 'direction', 'visible']